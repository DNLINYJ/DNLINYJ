### Hi there 👋

* I am the freshman of senior high school with learning programming.
* Completed a complete black box penetration test independently.
* Main programming language: Python C++
* Now I'm learning Unity and Reverse Engineering.

> [My Bilibili Channel](https://space.bilibili.com/100391403)

> Personal Blogs: https://blog.wtan.xyz/

——————————————————————————————————————————————————

* 我是一个会一点编程皮毛的苦逼高一人 ¯\\(ツ)/¯
* 曾独立完成完整黑盒渗透测试 ♪(´▽｀)
* 主语言：Python C++ ~还有一点点C#~

> 日常在 [Bilibili](https://space.bilibili.com/100391403) 活动

> 个人博客: https://blog.wtan.xyz/

[![菠萝小西瓜's GitHub stats](https://github-readme-stats.vercel.app/api?username=dnlinyj)](https://github.com/anuraghazra/github-readme-stats)
